module codeberg.org/gruf/go-sched

go 1.19

require codeberg.org/gruf/go-runners v1.6.3

require codeberg.org/gruf/go-errors/v2 v2.3.2 // indirect
