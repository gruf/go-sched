package sched_test

import (
	"sync/atomic"
	"testing"
	"time"

	"codeberg.org/gruf/go-sched"
)

const (
	tolerance = .20
	period    = time.Millisecond
	logAll    = false
)

func isTolerated(expect, d time.Duration) bool {
	return float64(expect)*(1-tolerance) <= float64(d) &&
		float64(d) <= float64(expect)*(1+tolerance)
}

func TestSchedulerRunOnce(t *testing.T) {
	var scheduler sched.Scheduler

	// Prepare test scheduler
	scheduler.Start(nil)
	defer scheduler.Stop()

	wait := make(chan struct{})
	defer close(wait)

	// Get initial time
	last := time.Now()

	job := sched.NewJob(func(now time.Time) {
		// Wait on chan signal
		if _, ok := <-wait; !ok {
			return
		}

		// Get time difference between runs
		diff := now.Sub(last)
		last = now

		if logAll {
			t.Logf("running job: %v", now)
		}

		// Check that received time is +-5% of expected
		if !isTolerated(period, diff) {
			t.Fatal("job outside of tolerance")
		}

		// Done
		<-wait
	}).At(last.Add(period))

	// Run job on the schedulerr
	cancel := scheduler.Schedule(job)
	defer cancel()

	// Wait for expected
	wait <- struct{}{}
	wait <- struct{}{}
}

func TestSchedulerRunRecurring(t *testing.T) {
	const expect = 100

	var scheduler sched.Scheduler

	// Prepare test scheduler
	scheduler.Start(nil)
	defer scheduler.Stop()

	var outside atomic.Uint32

	wait := make(chan struct{})
	defer close(wait)

	// Get initial time
	last := time.Now()

	job := sched.NewJob(func(now time.Time) {
		// Wait on chan signal
		if _, ok := <-wait; !ok {
			return
		}

		// Get time difference between runs
		diff := now.Sub(last)
		last = now

		if logAll {
			t.Logf("running job: %v", now)
		}

		// Check that received time is +-5% of expected
		if !isTolerated(period, diff) {
			outside.Add(1)
		}

		// Done
		<-wait
	}).Every(period)

	// Run job on the scheduler
	cancel := scheduler.Schedule(job)
	defer cancel()

	// Allow expect no. loops then return
	for i := 0; i < expect; i++ {
		wait <- struct{}{}
		wait <- struct{}{}
	}

	defer func() {
		const max = expect * tolerance
		outside := outside.Load()
		t.Logf("%d outside of tolerance", outside)
		if float64(outside) > max {
			t.Fatal("more than expected job runs outside of tolerance")
		}
	}()
}

func TestSchedulerRunMany(t *testing.T) {
	const count, expect = 100, 100

	var scheduler sched.Scheduler

	// Prepare test scheduler
	scheduler.Start(nil)
	defer scheduler.Stop()

	var outside atomic.Uint32

	wait := make(chan struct{}, count)
	defer close(wait)

	// Get initial time
	last := time.Now()

	job := sched.NewJob(func(now time.Time) {
		// Wait on chan signal
		if _, ok := <-wait; !ok {
			return
		}

		// Get time difference between runs
		diff := now.Sub(last)
		last = now

		if logAll {
			t.Logf("running job: %v", now)
		}

		// Check that received time is +-5% of expected
		if !isTolerated(period, diff) {
			outside.Add(1)
		}

		// Done
		<-wait
	}).Every(period)

	for i := 0; i < count; i++ {
		// Run job on the scheduler
		cancel := scheduler.Schedule(job)
		defer cancel()
	}

	// Allow expect no. loops then return
	for i := 0; i < expect; i++ {
		for i := 0; i < count; i++ {
			wait <- struct{}{}
			wait <- struct{}{}
		}
	}

	defer func() {
		const max = count * expect * tolerance
		outside := outside.Load()
		t.Logf("%d outside of tolerance", outside)
		if float64(outside) > max {
			t.Fatal("more than expected job runs outside of tolerance")
		}
	}()
}
