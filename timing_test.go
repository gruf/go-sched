package sched_test

import (
	"testing"
	"time"

	"codeberg.org/gruf/go-sched"
)

func TestOnce(t *testing.T) {
	now := time.Now()

	// Prepare once timing
	once := sched.Once(now)

	// Check that first invocation returns now time
	if !once.Next(now /* value doesn't matter */).Equal(now) {
		t.Fatal("once timing did not return now")
	}

	// Check that once has been reset to zero-time
	if !once.Next(now /* value doesn't matter */).Equal(time.Time{}) {
		t.Fatal("once timing did not reset after .Next()")
	}
}

func TestPeriodic(t *testing.T) {
	const diff = time.Millisecond

	now := time.Now()

	// Prepare perioding timing
	period := sched.Periodic(diff)

	for i := 0; i < 100; i++ {
		// Determine now + diff
		next := now.Add(diff)

		// Check that period returns now + diff
		if !period.Next(now).Equal(next) {
			t.Fatal("perioding timing did not return now + diff")
		}

		// Set next
		now = next
	}
}

func TestPeriodicAt(t *testing.T) {
	const diff = time.Millisecond

	now := time.Now()

	// Prepare PeriodicAt timing
	periodAt := sched.PeriodicAt{
		Once:   sched.Once(now),
		Period: sched.Periodic(diff),
	}

	// Check that first invocation returns now time
	if !periodAt.Next(now /* value doesn't matter */).Equal(now) {
		t.Fatal("periodic at timing did not return now on first call")
	}

	for i := 0; i < 100; i++ {
		// Determine now + diff
		next := now.Add(diff)

		// Check that period returns now + diff
		if !periodAt.Next(now).Equal(next) {
			t.Fatal("perioding timing did not return now + diff")
		}

		// Set next
		now = next
	}
}

func TestTimingWrap(t *testing.T) {
	const diff = time.Millisecond

	now := time.Now()
	once := sched.Once(now)

	// Prepare wrapping timing
	twrap := sched.TimingWrap{
		Outer: &once,
		Inner: sched.Periodic(diff),
	}

	// Check that first invocation returns now time
	if !twrap.Next(now /* value doesn't matter */).Equal(now) {
		t.Fatal("wrapped timing with once did not return now on first call")
	}

	for i := 0; i < 100; i++ {
		// Determine now + diff
		next := now.Add(diff)

		// Check that period returns now + diff
		if !twrap.Next(now).Equal(next) {
			t.Fatal("wrapped timing with perioding did not return now + diff")
		}

		// Set next
		now = next
	}
}
